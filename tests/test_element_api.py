import flask_unittest
from CourseManagerApp import create_app

class TestForAPI(flask_unittest.ClientTestCase):
    app = create_app()

    #getting all elements from the api
    def test_get_elements(self, client):
        resp = client.get('/api/elements/')
        self.assertEqual(resp.status_code, 200)
        json = resp.json
        self.assertIsNotNone(json)
        self.assertIsNotNone(json['results'])
        self.assertIsNotNone(json['next_page'])
        self.assertIsNone(json['prev_page'])

    #posting a element to the api
    def test_post_element(self, client):
        resp = client.get('/api/elements/')
        self.assertEqual(resp.status_code, 200)
        
        elements = resp.json
        element = elements['results'][0]     
        element['element_name'] = 'post element test'
        element['element_criteria'] = 'new criteria test'
        element['competency_id'] = '11DA'
        
        resp = client.post('/api/elements/', json=element)
        self.assertEqual(resp.status_code, 201)
        
        
    def test_get_element(self, client):
        resp = client.get('/api/elements/7')
        self.assertEqual(resp.status_code, 200)
        json = resp.json
        self.assertIsNotNone(json)
        
    def test_delete_element(self, client):
        resp = client.delete('/api/elements/123') 
        self.assertEqual(resp.status_code, 204)
        
    def test_put_element(self, client):
        resp = client.get('/api/elements/')
        self.assertEqual(resp.status_code, 200)
        
        elements = resp.json
        element = elements['results'][0]
        element['id'] = '103'
        element['element_name'] = 'post element test'
        element['element_criteria'] = 'new criteria test'
        element['competency_id'] = '11DA'
        resp = client.put(f"/api/elements/82", json=element)
        
        self.assertEqual(resp.status_code, 201)
        
    def test_get_element_error(self, client):
        resp = client.get('/api/elements/00')
        self.assertEqual(resp.status_code, 404)
