from wtforms import ValidationError
from flask_login import LoginManager, UserMixin

class User(UserMixin):
    def __init__(self, email, password, name, user_group='member', id=None,is_blocked=None):
        if not isinstance(email, str):
            raise TypeError()
        if not isinstance(password, str):
            raise TypeError()
        if not isinstance(name, str):
            raise TypeError()
        if id and not isinstance(id, int):
            raise TypeError()
        self.id = id
        self.email = email
        self.password = password
        self.name = name
        self.user_group = user_group
        self.is_blocked = is_blocked

from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from wtforms.validators import DataRequired
from wtforms import EmailField, PasswordField, StringField, BooleanField, SelectField
class SignupForm(FlaskForm):
    name = StringField('Name', 
    validators=[DataRequired()])
    email = EmailField('Email', 
    validators=[DataRequired()])
    password = PasswordField('Password', 
    validators=[DataRequired()])
    avatar = FileField('Avatar',
    validators=[])

class LoginForm(FlaskForm):
    email = EmailField('Email', 
    validators=[DataRequired()])
    password = PasswordField('Password', 
    validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
class EditProfileForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    email = EmailField('Email', validators=[DataRequired()])
    avatar = FileField('Avatar', validators=[])


        
class EditProfileFormAdmin(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    email = EmailField('Email', validators=[DataRequired()])
    avatar = FileField('Avatar', validators=[])
    groups = SelectField('Groups', validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        super(EditProfileFormAdmin, self).__init__(*args, **kwargs)
        try:
            from .dbmanager import get_db
            self.groups.choices = [(g[0], g[0]) for g in get_db().get_groups()]
        except Exception:
            print('An error occurred when loading the db')
            self.groups.choices = []
            
class EditProfileFormAdminUser(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    email = EmailField('Email', validators=[DataRequired()])
    avatar = FileField('Avatar', validators=[])

class ChangePasswordForm(FlaskForm):
    old_password = PasswordField('Old Password', 
    validators=[DataRequired()])
    new_password = PasswordField('New Password', 
    validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', 
    validators=[DataRequired()])
    # code that validates the form so password matches
    def validate_confirm_password(form, field):
        if field.data != form.new_password.data:
            raise ValidationError("New password and confirm password must match.")
    