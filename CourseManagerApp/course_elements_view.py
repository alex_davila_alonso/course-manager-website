from flask import Blueprint, flash, render_template, redirect, request, url_for, abort
from flask_login import login_required
from CourseManagerApp.dbmanager import get_db
from CourseManagerApp.element import Element, AddElementForm, ElementDropDown
from .course import Course

bp = Blueprint('course-elements', __name__, url_prefix='/course-elements/')

@bp.route('/<string:course_id>', methods=['GET', 'POST'])
@login_required
def get_elements(course_id):
    #form for creating an element
    form = AddElementForm()
    drop_down_form = ElementDropDown()
    try:
        course_elements = get_db().get_course_elements(course_id)
        course = get_db().get_course(course_id)
        elements, page_prev, page_next = get_db().get_elements()
    except:
        flash("error in the database")
        abort(404)
    if request.method == 'POST':
        if form.validate_on_submit():
            try:
                if not course:
                    abort(404)
                competency = get_db().get_competency(form.competency_id.data)
                element = Element(form.element_order.data,
                              form.element_name.data,
                              form.element_criteria.data,
                              competency)
                total_hours = (course.lab_hours + course.theory_hours)*15
                for element in elements:
                    if element.element_name == form.element_name.data:
                        flash("Element already exists")
                        return redirect(url_for('course-elements.get_elements',course_id=course_id))
                new_elem = get_db().add_element(element)
                get_db().add_course_element(new_elem.id,course_id,total_hours)
                return redirect(url_for('course-elements.get_elements',course_id=course_id))

            except Exception as e:
                print(e)
                flash('Could not load databse')
                abort(404)
        elif drop_down_form.validate_on_submit() and 'submit_drop_down_form' in request.form:
            
            chosen_element_id = drop_down_form.element_id.data
            try:
                total_hours = (course.lab_hours + course.theory_hours) * 15
                get_db().add_course_element(chosen_element_id, course_id, total_hours)
                return redirect(url_for('course-elements.get_elements', course_id=course_id))
            except Exception as e:
                print(e)
                flash('Could not load database')
                abort(404)
    
    return render_template('course_elements.html',course=course, elements=course_elements, form=form,drop_down_form=drop_down_form, all_elements=elements)
    
