import os
from flask import Blueprint, flash, redirect, render_template, request, url_for, current_app, send_from_directory
from CourseManagerApp.dbmanager import get_db
from CourseManagerApp.user import EditProfileForm, EditProfileFormAdmin, EditProfileFormAdminUser, SignupForm, User
from flask_login import current_user, login_required
from werkzeug.security import check_password_hash, generate_password_hash

bp = Blueprint('admin', __name__, url_prefix='/admin')

@login_required
@bp.route('/dashboard/', methods=['GET', 'POST'])
def dashboard():
    users = get_db().get_all_users()
    return render_template('dashboard.html', users=users)



@bp.route('/delete-user/<user_email>', methods=['GET', 'POST'])
@login_required
def delete_user(user_email):
    get_db().delete_user(user_email)
    flash('User deleted successfully', 'success')
    return redirect(url_for('admin.dashboard'))
  
@bp.route('/add-user/', methods=['GET', 'POST'])
@login_required
def add_user():
    form = SignupForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            if get_db().get_user(form.email.data):
                flash("User already exists")
            else:
                file = form.avatar.data
                avatar_dir = os.path.join(current_app.config['IMAGE_PATH'], form.email.data)
                avatar_path = os.path.join(avatar_dir, 'avatar.png')
                if not os.path.exists(avatar_dir):
                    os.makedirs(avatar_dir)
                file.save(avatar_path)
                hash = generate_password_hash(form.password.data)
                user = User(form.email.data, hash, form.name.data)
                get_db().add_user(user)
                flash("User succesfully added")
    return render_template('add_user.html', form=form)
  
@bp.route('/edit-other-profile/<user_email>', methods=['GET', 'POST'])
@login_required
def edit_other_profile(user_email):
    db = get_db()
    user = db.get_user(user_email)
     
    if current_user.user_group == 'admin_gp':
        form = EditProfileFormAdmin()
    else:
        form = EditProfileFormAdminUser()
    if form.validate_on_submit():
        if form.avatar.data:
            avatar_dir = os.path.join(current_app.config['IMAGE_PATH'], form.email.data)
            avatar_path = os.path.join(avatar_dir, 'avatar.png')
            if not os.path.exists(avatar_dir):
                os.makedirs(avatar_dir)
            form.avatar.data.save(avatar_path)

        if current_user.user_group == 'admin_gp':
            db.update_profile_admin(user_email, form.name.data, form.email.data, form.groups.data)
        else:
            db.update_profile_admin(user_email, form.name.data, form.email.data, None)

        flash('Your profile has been updated.', 'success')
        return redirect(url_for('admin.dashboard'))
    form.name.data = user.name    
    form.email.data = user_email
    return render_template('edit_profile_admin.html', form=form, user_email=user_email)

       

@bp.route('/block-user/<user_email>', methods=['GET', 'POST'])
@login_required
def block_user(user_email):
    get_db().block_user(user_email)
    flash('User blocked successfully', 'success')
    return redirect(url_for('admin.dashboard'))

@bp.route('/unblock-user/<user_email>', methods=['GET', 'POST'])
@login_required
def unblock_user(user_email):
    get_db().unblock_user(user_email)
    flash('User unblocked successfully', 'success')
    return redirect(url_for('admin.dashboard'))

@bp.route('/groups/', methods=['GET', 'POST'])
@login_required
def show_groups():
    groups = get_db().get_groups()
    return render_template('dashboard.html', groups=groups)