window.onload = function() {
  const flashBlock = document.getElementById('flash_block');

  if (flashBlock) {
      setTimeout(function() {
          flashBlock.classList.add('fadeOut'); // Start fadeOut after 10 seconds
      }, 10000);

      // After the fadeOut animation ends, fully hide the flashBlock
      flashBlock.addEventListener('animationend', function() {
          flashBlock.style.visibility = 'hidden';
      });
  }
};
