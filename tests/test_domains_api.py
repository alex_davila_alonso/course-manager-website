import flask_unittest
from CourseManagerApp import create_app

class TestForAPI(flask_unittest.ClientTestCase):
    app = create_app()

    def test_get_domains(self, client):
        resp = client.get('/api/domains')
        self.assertEqual(resp.status_code, 200)
        json = resp.json
        self.assertIsNotNone(json)
    

    def test_add_domain(self, client):
        resp = client.get('/api/domains')
        self.assertEqual(resp.status_code, 200)
        domain = resp.json[0]

        domain["domain_name"] = "Unittest Domain"
        domain["domain_description"] = "Unittest description"

        resp = client.post('/api/domains', json=domain)
        self.assertEqual(resp.status_code, 201)

    def test_add_domain_duplicate(self, client):
        resp = client.get('/api/domains')
        self.assertEqual(resp.status_code, 200)
        domain = resp.json[0]

        domain["domain_name"] = "Unittest Domain"
        domain["domain_description"] = "Unittest description"
        
        resp = client.post('/api/domains', json=domain)
        self.assertEqual(resp.status_code, 400)

    def test_update_domain(self, client):
        resp = client.get('/api/domains')
        self.assertEqual(resp.status_code, 200)
        domain = resp.json[0]

        domain["domain_name"] = "Unittest Domain"
        domain["domain_description"] = "Unittest description"

        resp = client.put('/api/domains/21', json=domain)
        self.assertEqual(resp.status_code, 200)

    def test_update_domain_error(self, client):
        resp = client.get('/api/domains')
        self.assertEqual(resp.status_code, 200)
        domain = resp.json[0]

        domain["domain_name"] = "Unittest Domain"
        domain["domain_description"] = "Unittest description"

        resp = client.put('/api/domains/19923', json=domain)
        self.assertEqual(resp.status_code, 404)
        
    def test_delete_domain(self, client):
        resp = client.get('/api/domains/21')
        self.assertEqual(resp.status_code, 200)
        course = resp.json

        resp = client.delete('/api/domains/21', json=course)
        self.assertEqual(resp.status_code, 204)  

    def test_delete_domain_error(self, client):
        resp = client.get('/api/domains/21')
        self.assertEqual(resp.status_code, 200)
        course = resp.json

        resp = client.delete('/api/domains/1233', json=course)
        self.assertEqual(resp.status_code, 404)