import os
from flask import Blueprint, flash, redirect, render_template, request, url_for, current_app, send_from_directory
from CourseManagerApp.dbmanager import get_db
from CourseManagerApp.user import LoginForm, SignupForm, ChangePasswordForm, EditProfileForm ,User
from werkzeug.security import check_password_hash, generate_password_hash
from flask_login import current_user, login_user, logout_user, login_required


bp = Blueprint('auth', __name__, url_prefix='/auth/')

@bp.route('/signup/', methods=['GET', 'POST'])
def signup():
    form = SignupForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            if get_db().get_user(form.email.data):
                flash("User already exists")
            else:
                file = form.avatar.data
                if file is not None:
                    avatar_dir = os.path.join(current_app.config['IMAGE_PATH'], form.email.data)
                    avatar_path = os.path.join(avatar_dir, 'avatar.png')
                    if not os.path.exists(avatar_dir):
                        os.makedirs(avatar_dir)
                    file.save(avatar_path)
                else:
                    flash("No avatar uploaded")  # Optional: inform the user that no avatar was uploaded
                hash = generate_password_hash(form.password.data)
                user = User(form.email.data, hash, form.name.data)
                get_db().add_user(user)
                flash("User created, please log in.")
                return redirect(url_for('auth.login'))  # replace 'login' with the function name for your login route
    return render_template('signup.html', form=form)



@bp.route('/login/', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            #Check our user
            user = get_db().get_user(form.email.data)
            if user:
                # Check if blocked 
                if user.is_blocked:
                    flash("User is blocked, please contact your system administrator")
                    return redirect(url_for('auth.login'))
                #Check the password
                if check_password_hash(user.password, form.password.data):
                    #User can login
                    login_user(user, form.remember_me.data)
                    flash("Logged in successfully")
                    return redirect(url_for('home_view.index'))
                else:
                    flash("Incorrect password")
            else:
                flash("Incorrect Email")
        else:
            flash("Cannot login")
    return render_template('login.html', form=form)

@bp.route('/logout/')
@login_required
def logout():
    logout_user()
    flash("Logged out successfully")
    return redirect(url_for('auth.login'))

@bp.route('/profile/<email>')
@login_required
def profile(email):
    user = get_db().get_user(email)
    return render_template('profile.html', user=user)

@bp.route('/avatars/<email>/avatar.png')
@login_required
def show_avatar(email):
    path = os.path.join(current_app.config['IMAGE_PATH'], email)
    return send_from_directory(path, 'avatar.png')
    
    
@bp.route('/members/')
def get_members():
    members = get_db().get_all_member_users()
    return render_template('index.html', members=members)

@bp.route('/change-pass/', methods=['GET', 'POST'])
@login_required
def change_pass():
    form = ChangePasswordForm()
    if form.validate_on_submit():
        db = get_db()
        user = db.get_user(current_user.email)
        if user and check_password_hash(user.password, form.old_password.data):
            hash = generate_password_hash(form.new_password.data)
            db.update_password(current_user.email, hash)
            flash('Your password has been updated.', 'success')
            return redirect(url_for('home_view.index'))
        else:
            flash('Invalid old password.', 'provide your old password')
    return render_template('change_pass.html', form=form)


@bp.route('/edit-profile/', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm()
    if form.validate_on_submit():
        db = get_db()
        if form.avatar.data:
            avatar_dir = os.path.join(current_app.config['IMAGE_PATH'], form.email.data)
            avatar_path = os.path.join(avatar_dir, 'avatar.png')
            if not os.path.exists(avatar_dir):
                os.makedirs(avatar_dir)
            form.avatar.data.save(avatar_path)

        db.update_profile(current_user.email, form.name.data, form.email.data)
        flash('Your profile has been updated.', 'success')
        return redirect(url_for('auth.profile', email=current_user.email))

    form.name.data = current_user.name
    form.email.data = current_user.email
    return render_template('edit_profile.html', form=form)

