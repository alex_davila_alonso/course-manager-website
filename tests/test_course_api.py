import flask_unittest
from CourseManagerApp import create_app

class TestForAPI(flask_unittest.ClientTestCase):
    app = create_app()

    def test_get_courses(self, client):
        resp = client.get('/api/courses')
        self.assertEqual(resp.status_code, 200)
        json = resp.json
        self.assertIsNotNone(json)
        self.assertIsNotNone(json['results'])
        self.assertIsNotNone(json['next_page'])
        self.assertIsNone(json['prev_page'])

    def test_add_course(self, client):
        resp = client.get('/api/courses')
        self.assertEqual(resp.status_code, 200)
        course = resp.json['results'][0]

        course["course_id"] = "009-300-DW"
        course["course_title"] = "New Course II"
        course["description"] = "New Description."
        course["domain"] =  1
        course["lab_hours"] =  3
        course["term"] = 1
        course["theory_hours"] = 3
        course["work_hours"] = 3

        resp = client.post('/api/courses', json=course)
        self.assertEqual(resp.status_code, 201)

    def test_add_course_error(self, client):
        resp = client.get('/api/courses')
        self.assertEqual(resp.status_code, 200)
        course = resp.json['results'][0]

        course["course_id"] = "NotGoodID"
        course["course_title"] = "New Course II"
        course["description"] = "New Description."
        course["domain"] =  1
        course["lab_hours"] =  3
        course["term"] = 1
        course["theory_hours"] = 3
        course["work_hours"] = 3

        resp = client.post('/api/courses', json=course)
        self.assertEqual(resp.status_code, 400)

    def test_add_course_duplicate(self, client):
        resp = client.get('/api/courses')
        self.assertEqual(resp.status_code, 200)
        course = resp.json['results'][0]

        course["course_id"] = "123-123-TT"
        course["course_title"] = "New Course II"
        course["description"] = "New Description."
        course["domain"] =  1
        course["lab_hours"] =  3
        course["term"] = 1
        course["theory_hours"] = 3
        course["work_hours"] = 3

        resp = client.post('/api/courses', json=course)
        self.assertEqual(resp.status_code, 400)

    def test_update_course(self, client):
        resp = client.get('/api/courses')
        self.assertEqual(resp.status_code, 200)
        course = resp.json['results'][0]

        course["course_id"] = "009-300-DW"
        course["course_title"] = "Unit Test Title"
        course["description"] = "Unit Test Description"
        course["domain"] =  1
        course["lab_hours"] =  3
        course["term"] = 1
        course["theory_hours"] = 3
        course["work_hours"] = 3

        resp = client.put('/api/courses/009-300-DW', json=course)
        self.assertEqual(resp.status_code, 200)

    def test_update_course_error(self, client):
        resp = client.get('/api/courses')
        self.assertEqual(resp.status_code, 200)
        course = resp.json['results'][0]

        course["course_id"] = "009-300-DW"
        course["course_title"] = "Unit Test Title"
        course["description"] = "Unit Test Description"
        course["domain"] =  1
        course["lab_hours"] =  3
        course["term"] = 1
        course["theory_hours"] = 3
        course["work_hours"] = 3

        resp = client.put('/api/courses/000-000-SS', json=course)
        self.assertEqual(resp.status_code, 404)
        
    def test_delete_course(self, client):
        resp = client.get('/api/courses/420-110-DW')
        self.assertEqual(resp.status_code, 200)
        course = resp.json

        resp = client.delete('/api/courses/420-110-DW', json=course)
        self.assertEqual(resp.status_code, 204)

    def test_delete_course_error(self, client):
        resp = client.get('/api/courses/000-000-SS')
        self.assertEqual(resp.status_code, 404)
        course = resp.json

        resp = client.delete('/api/courses/000-000-HO', json=course)
        self.assertEqual(resp.status_code, 404)

    def test_course_exists(self, client):
        resp = client.get('/api/courses/009-300-DW')
        self.assertEqual(resp.status_code, 200)
    
    def test_course_does_not_exist(self, client):
        resp = client.get('/api/courses/000-000-NO')
        self.assertEqual(resp.status_code, 404)
        
    def test_get_course_elements(self, client):
        resp = client.get('/api/courses/420-110-DW/elements')
        self.assertEqual(resp.status_code, 200)
        json = resp.json
        self.assertIsNotNone(json)
    
    def test_add_course_elements(self, client):
        resp = client.get('/api/courses/420-110-DW/elements')
        self.assertEqual(resp.status_code, 200)
        elements = resp.json
        course_element = elements['course']['element'][0]

        course_element['course_id'] = '490-888-YA'
        course_element['element_id'] = 9
        course_element['element_hours'] = 99
        
        resp = client.post('/api/courses/420-110-DW/elements', json=course_element)
        self.assertEqual(resp.status_code, 201)
    
    def test_add_existing_course_elements_error(self, client):
        resp = client.get('/api/courses/420-110-DW/elements')
        self.assertEqual(resp.status_code, 200)
        elements = resp.json
        course_element = elements['course']['element'][0]

        course_element['course_id'] = '490-888-YA'
        course_element['element_id'] = 9
        course_element['element_hours'] = 99
        
        resp = client.post('/api/courses/420-110-DW/elements', json=course_element)
        self.assertEqual(resp.status_code, 409)
        
    def test_delete_course_element(self, client):
        resp = client.delete('/api/courses/420-110-DW/elements/11') 
        self.assertEqual(resp.status_code, 204)
    
    def test_delete_course_element_error(self, client):
        #attempting to delete non-existing element from a course
        resp = client.delete('/api/courses/420-110-DW/elements/82') 
        self.assertEqual(resp.status_code, 409)


