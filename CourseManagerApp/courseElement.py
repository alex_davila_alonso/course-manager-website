from .course import Course
from .element import Element
class CourseElement:
    def from_json(course_element_dict):
        return CourseElement(course_element_dict['course_id'],
            course_element_dict['element_id'],
            course_element_dict['element_hours']
            )

    def __init__(self, course, element, element_hours):
        if not isinstance(course, Course):
            raise TypeError("Course input must be of type Course")
        if not isinstance(element, Element):
            raise TypeError("Element input must be of type Element")
        if not isinstance(element_hours, float) or element_hours < 0:
            raise TypeError("Element hours must be of type float and a positive number")
        self.course = course
        self.element = element
        self.element_hours = element_hours
    
    def __repr__(self):
        return f'CourseElement({self.course}, {self.element})'
    
    def __str__(self):
        return f'<p> Course id: {self.course} </p> \
                <p> Element id: {self.element} </p>\
                <p> Element hours: {self.element_hours} </p>'
    def to_json(self):
        return {
            "course": self.course.id,
            "element": self.element.id,
            "element_hours": self.element_hours
        }

        
from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField
from wtforms.validators import DataRequired
class CourseElementForm(FlaskForm):
    course_id = StringField('course_id', validators=[DataRequired()])
    element_id = IntegerField('element_id', validators=[DataRequired()])


