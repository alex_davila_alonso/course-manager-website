import os
from flask import flash
import oracledb
from CourseManagerApp.user import User
from CourseManagerApp.competency import Competency
from CourseManagerApp.course import Course
from CourseManagerApp.courseElement import CourseElement
from CourseManagerApp.domain import Domain
from CourseManagerApp.element import Element
from CourseManagerApp.term import Term
import logging

logging.basicConfig(level=logging.DEBUG)

class Database:
    def __init__(self):
        self.__user =os.environ['DBUSER']
        self.__password = os.environ['DBPWD']
        self.__host = '198.168.52.211'
        self.__port = 1521
        self.__service_name = 'pdbora19c.dawsoncollege.qc.ca'
        self.__conn = oracledb.connect(user=self.__user,
                                       password=self.__password,
                                       host=self.__host, port=self.__port,
                                       service_name=self.__service_name)
        self.__conn.autocommit = True

    def run_file(self, file_path):
        statement_parts = []
        with self.__conn.cursor() as cursor:
            with open(file_path, 'r') as f:
                for line in f:
                    statement_parts.append(line)
                    if line.strip('\n').strip('\n\r').strip().endswith(';'):
                        statement = "".join(
                            statement_parts).strip().rstrip(';')
                        if statement:
                            try:
                                cursor.execute(statement)
                            except Exception as e:
                                print(e)
                        statement_parts = []

    def close(self):
        if self.__conn:
            self.__conn.close()
            self.__conn = None

    # following methods are to get objects related to courses in db
    def get_domains(self):
        domains = []
        with self.__conn.cursor() as cursor:
            results = cursor.execute(
                'select domain_id, domain, domain_description from domains')
            for row in results:
                domain = Domain(domain_name=row[1], domain_description=row[2])
                domain.domain_id = row[0]
                domains.append(domain)
        return domains

    def get_domain(self, domain_id):
        domain = None
        with self.__conn.cursor() as cursor:
            results = cursor.execute(
                'select domain_id, domain, domain_description from domains where domain_id = :domain_id', domain_id=domain_id)
            for row in results:
                domain = Domain(domain_name=row[1], domain_description=row[2])
                domain.domain_id = row[0]
        return domain

    def get_term(self, term_id):
        term = None
        with self.__conn.cursor() as cursor:
            results = cursor.execute(
                'select term_id, term_name from terms where term_id = :term_id', term_id=term_id)
            for row in results:
                term = Term(term_id=row[0], term_name=row[1])
        return term
    
    def get_terms(self):
        terms = []
        with self.__conn.cursor() as cursor:
            results = cursor.execute(
                'select term_id, term_name from terms')
            for row in results:
                term = Term(term_id=row[0], term_name=row[1])
                terms.append(term)
        return terms

    def get_courses(self, page_num=1, page_size=50, search_word=None):
        courses = []
        offset = (page_num-1)*page_size
        prev_page = None
        next_page = None
        results = None
        with self.__conn.cursor() as cursor:
            if search_word:
                results = cursor.execute("select count(*) from courses where course_id like '%' || :search_query || '%' or '%' || :search_query || '%' like '%' || :search_query || '%' or description like '%' || :search_query || '%'",\
                    search_query=search_word)
                count = results.fetchone()[0]
            else:
                results = cursor.execute('select count(*) from courses')
                count = results.fetchone()[0]

            if search_word:
                results = cursor.execute("select course_id, course_title, theory_hours, lab_hours, work_hours, \
                 description, domain_id, term_id from courses \
                 where course_id like '%' || :search_query || '%' or course_title like '%' || :search_query || '%'\
                 order by course_id offset :offset rows fetch next :page_size rows only",search_query=search_word,offset=offset,page_size=page_size)
            else:
                results = cursor.execute('select course_id, course_title, theory_hours, lab_hours, work_hours, \
                                     description, domain_id, term_id from courses order by course_id offset :offset rows fetch next :page_size rows only',\
                                     offset=offset, page_size=page_size)
            for row in results:
                domain = self.get_domain(row[6])
                term = self.get_term(row[7])
                course = Course(course_id=row[0], course_title=row[1], theory_hours=row[2], lab_hours=row[3],
                                work_hours=row[4], description=row[5], domain=domain, term=term)
                courses.append(course)
        if page_num > 1:
            prev_page = page_num - 1
        if len(courses) > 0 and (count/page_size) > page_num:
            next_page = page_num + 1 
        return courses, prev_page, next_page

    def get_course(self, course_id):
        course = None
        with self.__conn.cursor() as cursor:
            results = cursor.execute('select course_id, course_title, theory_hours, lab_hours, work_hours, \
                                     description, domain_id, term_id from courses where course_id = :course_id', course_id=course_id)
            for row in results:
                domain = self.get_domain(row[6])
                term = self.get_term(row[7])
                course = Course(course_id=row[0], course_title=row[1], theory_hours=row[2], lab_hours=row[3],
                                work_hours=row[4], description=row[5], domain=domain, term=term)

        return course

    # the following methods are to add objects related to courses to db
    def get_competencies(self, page_num=1, page_size=100, search_word=None):
        competencies = []
        offset = (page_num-1)*page_size
        prev_page=None
        next_page=None
        results = None
        with self.__conn.cursor() as cursor:
            
            if search_word:
                    #search the db for the search_word. If found, return the competency. 
                results = cursor.execute("select competency_id, competency, competency_achievement, competency_type from competencies\
                        where competency_id like '%' || :search_word || '%' \
                            or competency like '%' || :search_word || '%' \
                                or competency_achievement like '%' || :search_word || '%' \
                                    or competency_type like '%' || :search_word || '%' \
                                        order by competency_id offset :offset rows fetch next :page_size rows only",
                                        search_word=search_word, offset=offset, page_size=page_size)
                count = 150 #since pagination is only needed for the api, setting count to pre-defined value.
            else: 
                results = cursor.execute('select count(*) from competencies')
                count = results.fetchone()[0]
                results = cursor.execute('select competency_id, competency, competency_achievement, competency_type from competencies order by competency_id \
                    offset :offset rows fetch next :page_size rows only', offset=offset, page_size=page_size)
            for row in results:
                competency = Competency(competency_id=row[0], competency_name=row[1], competency_achievement=row[2], competency_type=row[3])
                competencies.append(competency)
        if page_num > 1:
            prev_page = page_num - 1
        if len(competencies) > 0 and (count/page_size) > page_num:
            next_page = page_num + 1
        return competencies, prev_page, next_page
    
    def get_competency(self, competency_id):
        if not isinstance(competency_id, str):
            raise TypeError
        try:
            with self.__conn.cursor() as cursor:
                results = cursor.execute('select competency_id, competency, competency_achievement, competency_type from competencies where competency_id=:competency_id', competency_id=competency_id)
                for row in results:
                    competency = Competency(competency_id=row[0], competency_name=row[1], competency_achievement=row[2], competency_type=row[3])
                    return competency
            return None
        except oracledb.Error as e:
            #exception if case that db connection is closed
            raise RuntimeError("Database connection is closed.")
    
    def get_elements(self, page_num=1, page_size=100, search_word=None):
        elements = []
        offset = (page_num-1)*page_size
        prev_page=None
        next_page=None

        try:
            with self.__conn.cursor() as cursor:
                if search_word:
                    results = cursor.execute("select element_id, element_order, element, element_criteria, competency_id from elements\
                        where element_id like '%' || :search_word || '%' \
                            or element_order like '%' || :search_word || '%'\
                                or element like '%' || :search_word || '%'\
                                    or element_criteria like '%' || :search_word || '%'\
                                        or competency_id like '%' || :search_word || '%' \
                                            order by competency_id offset :offset rows fetch next :page_size rows only",
                                            search_word=search_word, offset=offset, page_size=page_size)
                    count = 150 #since pagination only needed for api, set count to pre-defined value

                else:
                    results = cursor.execute('select count(*) from elements')
                    count = results.fetchone()[0]

                    results = cursor.execute('select element_id, element_order, element, element_criteria, competency_id from elements order by element_id \
                        offset :offset rows fetch next :page_size rows only', offset=offset, page_size=page_size)

                for row in results:
                    #select the connect competency from db to add it to the element object
                    competency = self.get_competency(competency_id=row[4])
                    element = Element(element_order=row[1], element_name=row[2], element_criteria=row[3], competency=competency)
                    element.id = row[0]
                    elements.append(element)
                if page_num > 1:
                    prev_page = page_num - 1
                if len(elements) > 0 and (count/page_size) > page_num:
                    next_page = page_num + 1
            return elements, prev_page, next_page
        except oracledb.Error as e:
            #exception if case that db connection is closed
            raise RuntimeError("Database connection is closed.")
    
    def get_element(self, element_id):
        if not isinstance(element_id, int):
            raise TypeError("Element ID must be an integer")
        try:
            with self.__conn.cursor() as cursor:
                cursor.execute('SELECT element_id, element_order, element, element_criteria, competency_id FROM elements WHERE element_id = :element_id', {'element_id': element_id})
                row = cursor.fetchone()
                if row is None:
                    return None
                else:
                    # get the competency associated to the element
                    competency = self.get_competency(competency_id=row[4])
                    element = Element(element_order=row[1], element_name=row[2], element_criteria=row[3], competency=competency)
                    element.id = row[0]
                    return element
        except oracledb.Error as e:
            # exception in case that db connection is closed
            raise RuntimeError("Database connection is closed.")

    def get_element_by_id(self, elem_id):
        if not isinstance(elem_id, int):
            raise TypeError
        try:
            with self.__conn.cursor() as cursor:
                results = cursor.execute('select element_id, element_order, element, element_criteria, competency_id from elements where element_id=:element_id', element_id=elem_id)
                for row in results:
                    #get the competency associated to the element
                    competency = self.get_competency(competency_id=row[4])
                    element = Element(element_order=row[1], element_name=row[2], element_criteria=row[3], competency=competency)
                    element.id = row[0]
                    return element
            return None
        except oracledb.Error as e:
            #exception if case that db connection is closed
            print(e)
            raise RuntimeError("Database connection is closed.")

    def get_course_elements(self, course_id):
        elements = []
        try:
            with self.__conn.cursor() as cursor:
                results = cursor.execute('select course_id, element_id, element_hours from courses_elements where course_id = :course_id', course_id=course_id)
                for row in results:
                    element_id = row[1]
                    element = self.get_element_by_id(element_id)
                    elements.append(element)
            return elements
        except oracledb.Error as e:
            #exception if case that db connection is closed
            raise RuntimeError("Database connection is closed.")  
        
    def get_all_course_elements(self):
        course_elements = []
        try:
            with self.__conn.cursor() as cursor:
                results = cursor.execute('select course_id, element_id, element_hours from courses_elements')
                for row in results:
                    course = self.get_course(course_id=row[0])
                    element_id = row[1]
                    element_hours = float(row[2])
                    element = self.get_element_by_id(element_id)
                    course_element = CourseElement(course, element, element_hours)
                    course_elements.append(course_element)
            return course_elements
        except oracledb.Error as e:
            #exception if case that db connection is closed
            raise RuntimeError("Database connection is closed.")  
     


    def get_competency_elements(self, competency_id):
        elements = []
        # try:
        with self.__conn.cursor() as cursor:
            results = cursor.execute('select element_id, element_order, element, element_criteria, competency_id from elements where competency_id=:competency_id', competency_id=competency_id)
            for row in results:
                element_id = row[0]
                element = self.get_element_by_id(element_id)
                elements.append(element)
        return elements
        # except oracledb.Error as e:
        #     #exception if case that db connection is closed
        #     raise RuntimeError("Database connection is closed.")       

    def add_competency(self, competency):
        if not isinstance(competency, Competency):
            raise TypeError()

        try:
            with self.__conn.cursor() as cursor:
                cursor.execute('insert into competencies (competency_id, competency, competency_achievement, competency_type)\
                                values (:competency_id, :competency_name, :competency_achievement, :competency_type)',
                competency_id = competency.id,
                competency_name = competency.competency_name,
                competency_achievement = competency.competency_achievement,
                competency_type = competency.competency_type)
        except oracledb.Error as e:
            #exception if case that db connection is closed
            print(e)
            raise RuntimeError("Database connection is closed.")
        
    def edit_competency(self, competency_to_edit, competency_new):
        if not isinstance(competency_to_edit, Competency) or not isinstance (competency_new, Competency):
            raise TypeError()
        try: 
            with self.__conn.cursor() as cursor:
                results = cursor.execute('update Competencies set \
                                         COMPETENCY=:new_name, \
                                         COMPETENCY_ACHIEVEMENT=:new_achievement, \
                                         COMPETENCY_TYPE=:new_type\
                                         where competency_ID=:old_id',
                                         new_name=competency_new.competency_name,
                                         new_achievement=competency_new.competency_achievement,
                                         new_type=competency_new.competency_type, 
                                         old_id=competency_to_edit.id)
        except oracledb.Error as e:
            print(e)
            # Reraise the exception with a custom error message
            raise RuntimeError("Database encountered and error")
        
    def delete_competency(self, competency_to_delete):
        if not isinstance(competency_to_delete, Competency):
            raise TypeError()
        try: 
            with self.__conn.cursor() as cursor:
                cursor.execute('delete from competencies \
                                where competency_id=:competency_id', 
                                 competency_id=competency_to_delete.id)
        except oracledb.Error as e:
            print(e)
            # Reraise the exception with a custom error message
            raise RuntimeError("Database encountered and error")

    def add_element(self, element):
        try:
            with self.__conn.cursor() as cursor:
                cursor.execute('insert into elements (element_order, element, element_criteria, competency_id) \
                            values(:element_order, :element_name, :element_criteria, :competency_id)',
                            element_order = element.element_order,
                            element_name = element.element_name,
                            element_criteria = element.element_criteria,
                            competency_id = element.competency.id)
                results = cursor.execute('select element_id, element_order, element, element_criteria, competency_id from elements where element=:element', element=element.element_name)
                for row in results:
                    #get the competency associated to the element
                    competency = self.get_competency(competency_id=row[4])
                    element = Element(element_order=row[1], element_name=row[2], element_criteria=row[3], competency=competency)
                    element.id = row[0]
        except oracledb.Error as e:
            #exception if case that db connection is closed
            raise RuntimeError("Database connection is closed.")
        return element
    
    def edit_element(self, element_to_edit, element_new):
        if not isinstance(element_to_edit, Element) or not isinstance (element_new, Element):
            raise TypeError()
        try: 
            with self.__conn.cursor() as cursor:
                results = cursor.execute('update ELEMENTS set ELEMENT_ORDER=:new_order,\
                                         ELEMENT=:new_name, \
                                         ELEMENT_CRITERIA=:new_criteria, \
                                         COMPETENCY_ID=:new_comp_id\
                                         where ELEMENT_ID=:old_id', 
                                         new_order=element_new.element_order,
                                         new_name=element_new.element_name,
                                         new_criteria=element_new.element_criteria,
                                         new_comp_id=element_new.competency.id, 
                                         old_id=element_to_edit.id)
        except oracledb.Error as e:
            print(e)
            # Reraise the exception with a custom error message
            raise RuntimeError("Database encountered and error")
        
    def delete_element(self, element_to_delete):
        if element_to_delete is None or not isinstance(element_to_delete, Element):
            raise TypeError("Element to delete is not of type Element or is None")
        try:
            with self.__conn.cursor() as cursor:
                cursor.execute('delete from elements where element_id=:element_id', 
                                element_id=element_to_delete.id)
                self.__conn.commit()  # Commit the changes
        except oracledb.Error as e:
            # Reraise the exception with a custom error message
            raise RuntimeError("Database encountered an error")




    
    def add_course_element(self, elem_id, course_id, elem_hours):
        try:
            with self.__conn.cursor() as cursor:
                cursor.execute('insert into courses_elements (course_id, element_id, element_hours)\
                            values (:course_id, :element_id, :element_hours)',
                            course_id = course_id, 
                            element_id = elem_id, 
                            element_hours = elem_hours)
        except oracledb.Error as e:
            #exception if case that db connection is closed
            raise RuntimeError("Database connection is closed.")
        
    def delete_course_element(self, element_id, course_id):
        try:
            with self.__conn.cursor() as cursor:
                cursor.execute('delete from courses_elements where course_id=:course_id and element_id=:element_id',
                            course_id = course_id, 
                            element_id = element_id)
        except oracledb.Error as e:
            #exception if case that db connection is closed
            raise RuntimeError("Database connection is closed.")

    def add_domain(self, domain):
        if not isinstance(domain, Domain):
            raise TypeError()
        with self.__conn.cursor() as cursor:
            cursor.execute('insert into domains (domain, domain_description) values(:domain, :domain_description)',
                           domain=domain.domain_name,
                           domain_description=domain.domain_description)
            results = cursor.execute(
                'select domain_id, domain, domain_description from domains where domain = :domain', domain=domain.domain_name)
            for row in results:
                domain = Domain(domain_name=row[1], domain_description=row[2])
                domain.domain_id = row[0]
        return domain

    def delete_domain(self, domain_id):
        with self.__conn.cursor() as cursor:
            try:
                cursor.execute(
                    'delete from courses where domain_id = :domain_id', domain_id=domain_id)
                cursor.execute(
                    'delete from domains where domain_id = :domain_id', domain_id=domain_id)
                self.__conn.commit()
            except Exception as e:
                self.__conn.rollback()
                raise e

    def update_domain(self, domain):
        with self.__conn.cursor() as cursor:
            try:
                cursor.execute(
                    'update domains set domain = :domain, domain_description = :domain_description where domain_id = :domain_id',
                    domain=domain.domain_name, domain_description=domain.domain_description, domain_id=domain.domain_id)
                self.__conn.commit()
            except Exception as e:
                self.__conn.rollback()
                raise e

    def add_course(self, course):
        if not isinstance(course, Course):
            raise TypeError()
        with self.__conn.cursor() as cursor:
            cursor.execute('insert into courses (course_id, course_title, theory_hours, lab_hours, work_hours, description, domain_id, term_id) \
                        values (:course_id, :course_title, :theory_hours, :lab_hours, :work_hours, :description, :domain_id, :term_id)',
                        course_id=course.id,
                        course_title=course.course_title,
                        theory_hours=course.theory_hours,
                        lab_hours=course.lab_hours,
                        work_hours=course.work_hours,
                        description=course.description,
                        domain_id=self.get_domain(course.domain.domain_id).domain_id,
                        term_id=self.get_term(course.term.id).id)

    def delete_course(self, course_id):
        with self.__conn.cursor() as cursor:
            try:
                cursor.execute(
                    'delete from courses where course_id = :course_id', course_id=course_id)
                self.__conn.commit()
            except Exception as e:
                self.__conn.rollback()
                raise e

    def update_course(self, course):
        with self.__conn.cursor() as cursor:
            try:
                cursor.execute(
                    'update courses set course_id = :course_id, course_title = :course_title, theory_hours = :theory_hours, lab_hours = :lab_hours, work_hours = :work_hours,\
                         description = :description, domain_id = :domain_id, term_id = :term_id where course_id = :course_id',
                    course_id=course.id,
                    course_title=course.course_title,
                    theory_hours=course.theory_hours,
                    lab_hours=course.lab_hours,
                    work_hours=course.work_hours,
                    description=course.description,
                    domain_id=self.get_domain(course.domain.domain_id).domain_id,
                    term_id=self.get_term(course.term.id).id)
                self.__conn.commit()
            except Exception as e:
                self.__conn.rollback()
                raise e
                

    def add_user(self, user):
        if not isinstance(user, User):
            raise TypeError()
        try:
            with self.__conn.cursor() as cursor:
                cursor.execute('insert into proj_users (email, password, name, user_group,is_blocked) values (:email, :password, :name, :user_group, :is_blocked)',
                            email = user.email,
                            password = user.password,
                            name = user.name,
                            user_group=user.user_group,
                            is_blocked=0)
        except oracledb.Error as e:
            # Reraise the exception with a custom error message
            raise RuntimeError("Database connection is closed.")
        
    def get_all_users(self):
        try:
            with self.__conn.cursor() as cursor:
                results = cursor.execute('select id, email, password, name, user_group, is_blocked from proj_users')
                users = []
                for row in results:
                    user = User(id=row[0], email=row[1],
                        password=row[2], name=row[3], user_group=row[4], is_blocked=row[5])
                    users.append(user)
                return users
        except oracledb.Error as e:
            # Reraise the exception with a custom error message
            raise RuntimeError("Database connection is closed.")
    
    def get_all_member_users(self):
        try:
            with self.__conn.cursor() as cursor:
                results = cursor.execute('select id, email, password, name, user_group, is_blocked from proj_users where user_group=:user_group', user_group='member')
                users = []
                for row in results:
                    user = User(id=row[0], email=row[1],
                        password=row[2], name=row[3], user_group=row[4], is_blocked=row[5])
                    users.append(user)
                return users
        except oracledb.Error as e:
            # Reraise the exception with a custom error message
            raise RuntimeError("Database connection is closed.")
        
    def get_all_user_admins(self):
        try:
            with self.__conn.cursor() as cursor:
                results = cursor.execute('select id, email, password, name, user_group, is_blocked from proj_users where user_group=:user_group', user_group='admin_user_gp')
                users = []
                for row in results:
                    user = User(id=row[0], email=row[1],
                        password=row[2], name=row[3], user_group=row[4], is_blocked=row[5])
                    users.append(user)
                return users
        except:
            # Reraise the exception with a custom error message
            raise RuntimeError("Database connection is closed.")
        
    def get_all_admins(self):
        try:
            with self.__conn.cursor() as cursor:
                results = cursor.execute('select id, email, password, name, user_group, is_blocked from proj_users where user_group=:user_group', user_group='admin_gp')
                users = []
                for row in results:
                    user = User(id=row[0], email=row[1],
                        password=row[2], name=row[3], user_group=row[4], is_blocked=row[5])
                    users.append(user)
                return users
        except:
            # Reraise the exception with a custom error message
            raise RuntimeError("Database connection is closed.")
        
    def get_user_by_id(self, id):
        if not isinstance(id, int):
            raise TypeError()
        try:
            with self.__conn.cursor() as cursor:
                results = cursor.execute('select id, email, password, name, user_group, is_blocked from proj_users where id=:id', id=id)
                for row in results:
                    user = User(id=row[0], email=row[1],
                        password=row[2], name=row[3], user_group=row[4], is_blocked=row[5])
                    return user
            return None
        except oracledb.Error as e:
            # Reraise the exception with a custom error message
            raise RuntimeError("Database connection is closed.")

    def get_user(self, email):
        if not isinstance(email, str):
            raise TypeError()
        try:
            with self.__conn.cursor() as cursor:
                results = cursor.execute('select id, email, password, name, user_group,is_blocked from proj_users where email=:email', email=email)
                for row in results:
                    user = User(id=row[0], email=row[1],
                        password=row[2], name=row[3], user_group=row[4], is_blocked=row[5])
                    return user
            return None
        except oracledb.Error as e:
            # Reraise the exception with a custom error message
            raise RuntimeError("Database connection is closed.")
        
    def update_password(self, email, new_password):
        if not isinstance(email, str) or not isinstance(new_password, str):
            raise TypeError()
        try:
            with self.__conn.cursor() as cursor:
                cursor.execute("update proj_users set password=:password where email=:email",
                            password=new_password, email=email)
                self.__conn.commit()
        except oracledb.Error as e:
            raise RuntimeError("Error updating password.")
    
    def update_profile(self, old_email, name, email):
        try:
            with self.__conn.cursor() as cursor:
                cursor.execute("update proj_users set name=:name, email=:email where email=:old_email",
                            name=name, email=email, old_email=old_email)
                self.__conn.commit()
        except oracledb.Error as e:
            raise RuntimeError("Error updating profile.")

        
    def update_profile_admin(self, old_email, name, email, user_group=None):
        try:
            with self.__conn.cursor() as cursor:
                if user_group is None:
                    cursor.execute("update proj_users set name=:name, email=:email where email=:old_email",
                                name=name, email=email, old_email=old_email)
                else:
                    cursor.execute("update proj_users set name=:name, email=:email, user_group=:user_group where email=:old_email",
                                name=name, email=email, old_email=old_email, user_group=user_group)
                self.__conn.commit()
        except oracledb.Error as e:
            raise RuntimeError("Error updating profile.")

        
    # def update_profile_admin(self, old_email, name, email):
    #     try:
    #         with self.__conn.cursor() as cursor:
    #             cursor.execute("update proj_users set name=:name, email=:email where email=:old_email",
    #                         name=name, email=email, old_email=old_email)
    #             self.__conn.commit()
    #     except oracledb.Error as e:
    #         raise RuntimeError("Error updating profile.")

    def delete_user(self, email):
        with self.__conn.cursor() as cursor:
            cursor.execute("delete from proj_users where email=:email", email=email)
            self.__conn.commit()
            
    # not sure on how to block yet, will need to discuss with team and teacher, but code would look somethingl ike this (new column in db needed)
    def block_user(self, email):
        with self.__conn.cursor() as cursor:
            cursor.execute("update proj_users set is_blocked=1 where email=:email", email=email)
            self.__conn.commit()

    def unblock_user(self, email):
        with self.__conn.cursor() as cursor:
            cursor.execute("update proj_users set is_blocked=0 where email=:email", email=email)
            self.__conn.commit()
            
    def get_groups(self):
        try:
            with self.__conn.cursor() as cursor:
                results = cursor.execute('select distinct user_group from proj_users')
                groups = []
                for row in results:
                    groups.append(row)
                return groups
        except:
            # Reraise the exception with a custom error message
            raise RuntimeError("Database connection is closed.")

if __name__ == '__main__':
    conn = oracledb.connect(
        user=os.environ['DBUSER'],
        password=os.environ['DBPWD'],
        host='198.168.52.211',
        port=1521,
        service_name='pdbora19c.dawsoncollege.qc.ca')

    conn.close()
