class Domain:
    def from_json(domains_dict):
        return Domain(
            domains_dict['domain_name'],
            domains_dict['domain_description']
            )
            
    def __init__(self, domain_name, domain_description):
        if (not isinstance(domain_name, str) or domain_name.isspace() or domain_name == "" ):
            raise TypeError("Domain name must be a string")
        if (not isinstance(domain_description, str) or domain_description.isspace() or domain_description == "" ):
            raise TypeError("Domain description must be a string")
        self.id = None
        self.domain_name = domain_name
        self.domain_description = domain_description
    
    @property
    def domain_id(self):
        return self.id
    
    @domain_id.setter
    def domain_id(self, value):
        self.id = value

    def get_by_id(self, domain_id):
        from .dbmanager import get_db
        domain = get_db().get_domain(domain_id)
        return domain

    def __repr__(self):
        return f'{self.domain_name}, {self.domain_description})'
    
    def __str__(self):
        return f'<p> Domain name: {self.domain_name} </p>\
                <p> Domain description: {self.domain_description}</p> '

    def to_json(self):
        return {
            "domain_id": self.domain_id,
            "domain_name": self.domain_name,
            "domain_description": self.domain_description
        }

    
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired, Regexp
class DomainForm(FlaskForm):
    domain_name = StringField('Domain Name', validators=[DataRequired(), Regexp('^[^0-9]*$', message='Domain name cannot contain numbers.')])
    domain_description = StringField('Domain Description', validators=[DataRequired()])


