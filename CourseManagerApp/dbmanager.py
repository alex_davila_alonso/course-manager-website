from flask import g, current_app
from CourseManagerApp.db import Database

def get_db():
    if 'db' not in g:
        g.db = Database()
    return g.db

def close_db(_):
    db = g.pop('db', None)
    if db is not None:
        db.close()
