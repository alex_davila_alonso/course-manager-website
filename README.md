IP ADDRESS: 10.172.11.245:8000

Setting Up the Development Environmet:
Have python installed if not already
Create the virtual environment for the project (e.g., python -m venv .venv)
 pip install all libraries and freeze them in -r requirements.txt
Set DBUSER and set DBPWD for the PDBORA19C database
Setup database using schemal.sql
Run the flask app for debugging using flask --app CourseManagerApp --debug run


Deployment Setup:
Create the virtual environment for the project (e.g., python -m venv .venv)
Set DBUSER and set DBPWD for the PDBORA19C database
Setup database using schemal.sql
Allow the port through your firewall (e.g., ufw allow 8000)
Create a config.py with the SECRET_KEY setting
Run the command gunicorn -w 2 -b 0.0.0.0:8000 'CourseManagerApp:create_app()'


Project Setup:


Objects:
Create and set up the necessary objects for the project, such as Courses, Domains, Competencies, and Elements.

Templates:
Set up the required HTML templates for your Flask routes. These templates will be used to render the website's user interface.

Static Files:
Add any necessary CSS and JavaScript files to the project. These files will be used to style and add interactivity to the website.

Blueprints and Views:
Set up any necessary Flask Blueprints and views for the project. Blueprints help organize your application by separating different functionalities, while views are used to render the templates.

API:
Implement the required API endpoints and their respective HTTP requests. This will allow your application to communicate with external services and process data.

Tests:
Write and set up unit tests for the API endpoints to ensure that they work correctly and efficiently. These tests will help maintain the project's quality and prevent potential issues.

If you are using the users that come with the DB, for all the user that are part of the admins, their password will be Python420, and regular users its a123 e.g.:
instructor@dawsoncollege.qc.ca pass: Python420
vitaliy@gmail.com pass: a123

Usage Instructions:

Before doing anything it is recommended to login, to have access to more features in the website

There you can go to 4 different page from the home page which are: Courses, Elements, Competencies, and Home

in Course you will see a list of courses, you will have the option to add new courses delete or edit already existing ones. There will be a button "See Course Elements" that will take you to a age which displays the elements for a single course, from there you will be able to add elements to that course (if you want to delete or edit elements you can go to the elements page). From the course elements you will be able to visist the individual competency for an element, when you click the ID you will be taken to a page that displays the competency with all the elements, you will also have the option to add elements to it (if you want to delete or edit elements for a competency visit the elements page)

The individual Elements and Competencies pages let you manage all elements and competencies you will be able to delete, edit or add from them

As for the admin dahsboard, you have access to certain feature that regular users do not. If you are logged in under the intructor, you can manage users, block them, unblock etc, you can also change their group, which will in turn change their permissions. 