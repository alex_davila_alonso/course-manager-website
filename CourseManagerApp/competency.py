class Competency:
    def from_json(competency_dict):
        return Competency(competency_dict['id'],
            competency_dict['competency_name'],
            competency_dict['competency_achievement'],
            competency_dict['competency_type'],
            )
    def __init__(self, competency_id, competency_name, competency_achievement, competency_type):
        if (not isinstance(competency_name, str) or competency_name.isspace() or competency_name == "" ):
            raise TypeError("Competency name must be a string")
        if (not isinstance(competency_achievement, str) or competency_achievement.isspace() or competency_achievement == "" ):
            raise TypeError("Competency achievement must be a string")
        if (not isinstance(competency_type, str) or competency_type.isspace() or competency_type == "" ):
            raise TypeError("Competency type name must be a string")
        self.id = competency_id
        self.competency_name = competency_name
        self.competency_achievement = competency_achievement
        self.competency_type = competency_type

    def __repr__(self):
        return f'Competency({self.id}, {self.competency_name}, {self.competency_achievement}, {self.competency_type})'
    
    def __str__(self):
        return f' Competency name: {self.competency_name} \
                 Achievement: {self.competency_achievement}  \
                 Type: {self.competency_type})'
    def to_json(self):
        return {
            "id": self.id,
            "competency_name": self.competency_name,
            "competency_achievement": self.competency_achievement,
            "competency_type": self.competency_type
        }

from flask_wtf import FlaskForm
from wtforms import StringField, SelectField
from wtforms.validators import DataRequired, Length, Regexp
class AddCompetencyForm(FlaskForm):
    competency_id = StringField('Competency ID:',
                                 validators=[DataRequired(),
                                            Length(min=1, max=4),
                                            Regexp('[0-9]{2}[A-Z]{2}', 
                                            message="Competency ID must be in the form 00AA")])
    competency_name = StringField('Competency Name:', 
                                validators=[DataRequired(),
                                Length(min=1, max=250)])
    competency_achievement = StringField('Competency Achievement:', 
                                        validators=[DataRequired(),
                                        Length(min=1, max=500)])
    competency_type = SelectField('Competency Type:', 
                        choices=[('mandatory', 'Mandatory'), ('optional', 'Optional')], 
                        validators=[DataRequired()])
    
class EditCompetencyForm(FlaskForm):
    competency_name = StringField('New Competency Name:', 
                                validators=[DataRequired(),
                                Length(min=1, max=250)])
    competency_achievement = StringField('New Competency Achievement:', 
                                        validators=[DataRequired(),
                                        Length(min=1, max=500)])
    competency_type = SelectField('New Competency Type:', 
                        choices=[('mandatory', 'Mandatory'), ('optional', 'Optional')], 
                        validators=[DataRequired()])

   


