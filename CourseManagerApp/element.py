from .competency import Competency
class Element:
    def from_json(element_dict):
        return Element(element_dict['element_order'],
            element_dict['element_name'],
            element_dict['element_criteria'],
            element_dict['competency_id'],
            )

    def __init__(self, element_order, element_name, element_criteria, competency):
        if not isinstance(element_order, int):
            raise TypeError("Element order name must be an int")
        if (not isinstance(element_name, str) or element_name.isspace() or element_name == "" ):
            raise TypeError("Element name name must be a non-empty string")
        if (not isinstance(element_criteria, str) or element_criteria.isspace() or element_criteria == "" ):
            raise TypeError("Element criteria achievement must be a non-empty string")
        if not isinstance(competency, Competency) :
            raise TypeError("Competency input must be of type competency")
        self.id = None
        self.element_order = element_order
        self.element_name = element_name
        self.element_criteria = element_criteria
        self.competency = competency

    def __repr__(self):
        return f'Element({self.id}, {self.element_order}, {self.element_name},\
              {self.element_criteria}, {self.competency})'
    
    def __str__(self):
        return f'<p> Element id: {self.id} </p> \
                <p> Element order: {self.element_order} </p>\
                <p> Element name: {self.element_name} </p> \
                <p> Element criteria: {self.element_criteria})</p> \
                <p> Competency id : {self.competency})</p> '
    
    def to_json(self):
        return {
            "element_id": self.id,
            "element_order": self.element_order,
            "element_name": self.element_name,
            "element_criteria": self.element_criteria,
            "competency_id": self.competency.id
        }

        
    def to_json(self):
        return {
            "element_id": self.id,
            "element_order": self.element_order,
            "element_name": self.element_name,
            "element_criteria": self.element_criteria,
            "competency_id": self.competency.id
        }


from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SelectField
from wtforms.validators import DataRequired, Length
class AddElementForm(FlaskForm):
    element_order = IntegerField('Element Order', validators=[DataRequired()])
    element_name = StringField('Element Name', validators=[DataRequired(), Length(max=250)])
    element_criteria = StringField('Element Criteria', validators=[DataRequired(), Length(max=500)])
    competency_id = SelectField('Competency Id:', validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        super(AddElementForm, self).__init__(*args, **kwargs)
        try:
            from .dbmanager import get_db
            from flask import flash
            competencies, prev_page, next_page = get_db().get_competencies()
            self.competency_id.choices = [(comp.id, comp.id) for comp in competencies]
        except Exception as e:
            print(e)
            flash("The database encoutered an error. Competency choices could not be loaded.")

class EditElementForm(FlaskForm):
    element_name = StringField('New Element Name:', validators=[DataRequired(), Length(max=250)])
    element_order = IntegerField('New Element Order:', validators=[DataRequired()])
    element_criteria = StringField('New Element Criteria:', validators=[DataRequired(), Length(max=500)])
    competency_id = SelectField('New Competency Id:', validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        super(EditElementForm, self).__init__(*args, **kwargs)
        try:
            from .dbmanager import get_db
            from flask import flash
            competencies, prev_page, next_page = get_db().get_competencies()
            self.competency_id.choices = [(comp.id, comp.id) for comp in competencies]
        except Exception as e:
            print(e)
            flash("The database encoutered an error. Competency choices could not be loaded.")

class ElementDropDown(FlaskForm):
    element_id = SelectField('Element:', validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        super(ElementDropDown, self).__init__(*args, **kwargs)
        try:
            from .dbmanager import get_db
            from flask import flash
            elements, prev_page, next_page = get_db().get_elements()
            self.element_id.choices = [(elem.id, elem.element_name) for elem in elements]
        except Exception as e:
            print(e)
            flash("The database encountered an error. Element choices could not be loaded.")