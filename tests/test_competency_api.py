import flask_unittest
from CourseManagerApp import create_app

class TestForAPI(flask_unittest.ClientTestCase):
    app = create_app()

    #getting all competencies from the api
    def test_get_competencies(self, client):
        resp = client.get('/api/competencies/')
        self.assertEqual(resp.status_code, 200)
        json = resp.json
        self.assertIsNotNone(json)
        self.assertIsNotNone(json['results'])
        self.assertIsNotNone(json['next_page'])
        self.assertIsNone(json['prev_page'])

    #posting a competency to the api
    def test_post_competency(self, client):
        resp = client.get('/api/competencies/')
        self.assertEqual(resp.status_code, 200)
        
        competencies = resp.json
        competency = competencies['results'][0]
        competency['id'] = '23CY'
        competency['competency_name'] = 'post competency test'
        competency['competency_achievement'] = 'new achievement test'
        competency['competency_type'] = 'mandatory'
        
        resp = client.post('/api/competencies/', json=competency)
        self.assertEqual(resp.status_code, 201)
        
        
    def test_get_competency(self, client):
        resp = client.get('/api/competencies/00Q2')
        self.assertEqual(resp.status_code, 200)
        json = resp.json
        self.assertIsNotNone(json)
        
    def test_delete_competency(self, client):
        resp = client.delete('/api/competencies/55AA') 
        self.assertEqual(resp.status_code, 204)
        
    def test_put_competency(self, client):
        resp = client.get('/api/competencies/')
        self.assertEqual(resp.status_code, 200)
        
        competencies = resp.json
        competency = competencies['results'][0]
        competency['id'] = '55AA'
        competency['competency_name'] = 'post competency test'
        competency['competency_achievement'] = 'new achievement test'
        competency['competency_type'] = 'mandatory'
        resp = client.put(f"/api/competencies/{competency['id']}", json=competency)
        
        self.assertEqual(resp.status_code, 201)
        
    def test_get_competency_error(self, client):
        resp = client.get('/api/competencies/00')
        self.assertEqual(resp.status_code, 404)
        

        
        
       