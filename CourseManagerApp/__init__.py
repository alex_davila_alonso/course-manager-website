import os
from flask import Flask, flash, render_template, current_app
import secrets
import click
from flask_login import LoginManager

from .dbmanager import close_db, get_db
def create_app(test_config=None):
    app = Flask(__name__)

    app.config.from_mapping(
        SECRET_KEY=secrets.token_urlsafe(32),
        IMAGE_PATH=os.path.join(app.instance_path, 'images')
    )
   

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    from .home_views import bp as home_bp
    app.register_blueprint(home_bp)

    from .course_creation_views import bp as courses_bp
    app.register_blueprint(courses_bp)

    from .domain_manager import bp as domains_bp
    app.register_blueprint(domains_bp)

    from .auth_views import bp as auth_bp
    app.register_blueprint(auth_bp)

    from .competency_views import bp as competency_bp
    app.register_blueprint(competency_bp)

    from .element_views import bp as element_bp
    app.register_blueprint(element_bp)

    @login_manager.user_loader
    def load_user(user_id):
        return get_db().get_user_by_id(int(user_id))

    from .competencies_api import bp as competenciesapi_bp
    app.register_blueprint(competenciesapi_bp)

    from .course_elements_api import bp as course_elementsapi_bp
    app.register_blueprint(course_elementsapi_bp)

    from .courses_api import bp as coursesapi_bp
    app.register_blueprint(coursesapi_bp)

    from .domains_api import bp as domainsapi_bp
    app.register_blueprint(domainsapi_bp)

    from .elements_api import bp as elementsapi_bp
    app.register_blueprint(elementsapi_bp)

    from .users_api import bp as usersapi_bp
    app.register_blueprint(usersapi_bp)
    
    from .admin_manager import bp as admin_bp
    app.register_blueprint(admin_bp)

    from .course_elements_view import bp as course_elem_bp
    app.register_blueprint(course_elem_bp)
    
    app.teardown_appcontext(close_db)

    @app.errorhandler(404)
    def page_not_found(error):
        return render_template('custom404.html'), 404

    app.cli.add_command(click_command)

    try:
        os.makedirs(app.instance_path)
    except FileExistsError:
        pass

    try:
        os.makedirs(app.config['IMAGE_PATH'])
    except FileExistsError:
        pass

    return app

@click.command('my-command')
def click_command():
    import os
    get_db().run_file(os.path.join(current_app.root_path, 'schemal.sql'))
    click.echo('Running schemal.sql')


