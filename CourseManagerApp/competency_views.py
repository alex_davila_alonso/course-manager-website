from flask import Blueprint, flash, render_template, redirect, request, url_for, abort
from flask_login import login_required
from CourseManagerApp.element import Element, AddElementForm
from CourseManagerApp.dbmanager import get_db
from CourseManagerApp.competency import Competency, AddCompetencyForm, EditCompetencyForm

bp = Blueprint('competencies', __name__, url_prefix='/competencies/')

@bp.route('/', methods=['GET', 'POST'])
def get_competencies():
    
    form = AddCompetencyForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            competency = Competency(form.competency_id.data,
                                    form.competency_name.data, 
                                    form.competency_achievement.data, 
                                    form.competency_type.data)
            try:
                flash("Competency was added successfully.")
                get_db().add_competency(competency)
                return redirect(url_for('competencies.get_competencies'))
            except Exception as e:
                print(e)
                flash('Something went wrong with the database. Competency not added.')
        else:
            flash('Invalid input')
    search_word = None
    if request.args.get('search_word'):
        search_word = request.args.get('search_word')
        search_word = search_word.split('/')[0]
        try:
            competencies, prev_page, next_page = get_db().get_competencies(search_word=search_word)
            if len(competencies) == 0:
                flash("The searched keyword was not found.")
        except Exception as e:
            flash('Could not find word.')
    else:
        try:
            competencies, prev_page, next_page = get_db().get_competencies()
        except Exception as e:
            flash('Could not load competencies')
            abort(404)
        if not competencies or len(competencies)==0:
            abort(404)

    return render_template('competencies.html', competencies=competencies, form=form)



@bp.route('/edit/<string:competency_id>', methods=['GET', 'POST'])
@login_required
def edit_competency(competency_id):
    
    form = EditCompetencyForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            #create the competency with the updated fields
            competency_new = Competency(competency_id,
                                    form.competency_name.data, 
                                    form.competency_achievement.data, 
                                    form.competency_type.data)
            try:
                #get the selected competency
                comp_to_edit = get_db().get_competency(competency_id)
                get_db().edit_competency(comp_to_edit, competency_new)
                return redirect(url_for('competencies.get_competencies'))
            except Exception as e:
                print(e)
                flash("Database Error. Competency could not be added.")
        else:
            flash('Invalid input')
    return render_template('edit_competency.html', form=form, competency_id=competency_id)

@bp.route('/delete/<string:competency_id>', methods=['GET', 'POST'])
@login_required
def delete_competency(competency_id):
    try:
        comp_to_delete = get_db().get_competency(competency_id)
        get_db().delete_competency(comp_to_delete)
        flash(f"Competency {competency_id} was successfully deleted")
    except Exception as e:
        print(e)
        flash("Database Error. Competency could not be deleted.")
    return redirect(url_for('competencies.get_competencies'))

@bp.route('/add/', methods=['GET', 'POST'])
def add_competency():
    form = AddCompetencyForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            competency = Competency(form.competency_id.data,
                                    form.competency_name.data, 
                                    form.competency_achievement.data, 
                                    form.competency_type.data)
            try:
                get_db().add_competency(competency)
                return redirect(url_for('competencies.get_competencies'))
            except Exception as e:
                print(e)
                flash('Something went wrong with the database. Competency not added.')
        else:
            flash('Invalid input')
    return render_template('create_competency.html', form=form)

    
@bp.route('/<string:competency_id>', methods=['GET', 'POST'])
@login_required
def get_competency(competency_id):
    form = AddElementForm()
    form.competency_id.data = competency_id
    try:
        competency = get_db().get_competency(competency_id)
        elements = get_db().get_competency_elements(competency_id)
    except Exception as e:
        print(e)
        flash('Could not load db')
        abort(404)
    rqs = request.method
    print(rqs)
    if request.method == 'POST':
          if form.validate_on_submit():
            new_element = Element(form.element_order.data,
                              form.element_name.data,
                              form.element_criteria.data,
                              competency)
            
            try:
                for element in elements:
                    if element.element_name == new_element.element_name:
                        flash("Element already exist")
                        return redirect(url_for('competencies.get_competency',competency_id=competency_id))
                get_db().add_element(new_element)
                flash("Element was added successfully.")
                return redirect(url_for('competencies.get_competency',competency_id=competency_id))
            except:
                flash('Error loading the db')
                return redirect(url_for('competencies.get_competency',competency_id=competency_id))
    return render_template('one_competency.html',competency=competency, elements=elements, form=form)
    
     
            