from distutils.errors import CompileError
from flask import Blueprint, abort, jsonify, request, flash
from .user import User
from .dbmanager import get_db
bp = Blueprint('users_api', __name__, url_prefix='/api/users')

@bp.route('/', methods=['GET', 'POST'])
def users_api():
    users = None
    try:
        users = get_db().get_users()
    except Exception:
        flash("An error occured while loading the database")
    if request.method == 'POST':
        result = request.json
        if result:
            user = User.from_json(result)
            get_db().add_user(user)
        else:
            abort(400)
    else:
        if request.args:
            name = request.args.get("name")
            for user in users:
                if user.name == name:
                    return jsonify(user.__dict__)
    
    json_users = [user.__dict__ for user in users]
    return jsonify(json_users)