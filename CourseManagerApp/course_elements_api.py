# from distutils.errors import CompileError
# from flask import Blueprint, abort, jsonify, request, flash, make_response, url_for, redirect

# from .domain import Domain
# from .dbmanager import get_db
# bp = Blueprint('/api/courses', __name__, url_prefix='/api/courses')

# @bp.route('', methods=['GET', 'POST'])
# def domains_api():
#     domains = None
#     resp = None
#     try:
#         domains = get_db().get_domains()
#     except Exception:
#         resp = make_response({"Id" : 500,
#                               "description": 
#                               "Database was not loaded properly"},
#                               500)
#     if request.method == 'POST':
#         result = request.json
#         if result:
#             domain = Domain.from_json(result)
#             get_db().add_domain(domain)
#         else:
#             resp = make_response({"Id" : 400,
#                               "description": 
#                               "Error adding the domain"},
#                               400)
#     else:
#         if request.args:
#             name = request.args.get("domain_name")
#             for domain in domains:
#                 if domain.domain_name == name:
#                     return jsonify(domain.to_json())
    
#     json_domains = [domain.to_json() for domain in domains]
#     if request.method == 'POST':
#         resp = make_response(json_domains, 201)
#     else:
#         resp = make_response(json_domains, 200)
#     return resp
    

# @bp.route('/<int:domain_id>')
# def domain_api(domain_id):
#     resp = None
#     try:
#         domain = get_db().get_domain(domain_id)
#         # exists = get_db().get_domain(domain_id)
#         if not domain:
#             resp = make_response({"Id" : 404,
#                               "description": 
#                               "Domain does not exist"},
#                               404)
#         else:
#             json_domain = domain.to_json()
#             resp = make_response(json_domain, 200)
#     except:
#         resp = make_response({"Id" : 500,
#                               "description": 
#                               "Database was not loaded properly"},
#                               500)
#     return resp

# @bp.route('/<int:domain_id>',methods=['PUT'])
# def update_api(domain_id):
#     resp = None
#     try:
#         exists = get_db().get_domain(domain_id)
#         if not exists:
#             resp = make_response({"Id" : 404,
#                               "description": 
#                               "Domain does not exist"},
#                               404)
#         else:
#             data = request.json
#             domain = Domain.from_json(data)
#             domain.domain_id = domain_id
#             get_db().update_domain(domain)
#             json_domain = domain.to_json()
#             resp = make_response(json_domain,200)
#     except:
#         resp = make_response({"Id" : 500,
#                               "description": 
#                               "Database was not loaded properly"},
#                               500)
#     return resp
# @bp.route('/<int:domain_id>',methods=['DELETE'])
# def delete_api(domain_id):
#     resp = None
#     try:
#         exists = get_db().get_domain(domain_id)
#         if not exists:
#             resp = make_response({"Id" : 404,
#                               "description": 
#                               "Domain does not exist"},
#                               404)
#         else:
#             get_db().delete_domain(domain_id)
#             resp = make_response({"Id" : 204,
#                             "description": 
#                             f"Course {domain_id} was successfully deleted"},
#                               204)
#     except Exception:
#         resp = make_response({"Id" : 500,
#                               "description": 
#                               "Database was not loaded properly"},
#                               500)
#     return resp

from flask import Blueprint, jsonify, request, url_for, make_response 
from CourseManagerApp.dbmanager import get_db
from CourseManagerApp.courseElement import CourseElement


bp = Blueprint('course_elements_api', __name__, url_prefix='/api/course_elements')
@bp.route('/', methods=['GET', 'POST'])
def course_elements_api():
    try:
        course_elements = get_db().get_all_course_elements()
        #json_courses = {'next_page': next_page_url, 'prev_page': prev_page_url, 'results': [course.to_json() for course in courses]}
        #resp = make_response(json_courses, 200)
        json_course_elements = {'results': [course_element.to_json() for course_element in course_elements]}
        resp = make_response(json_course_elements, 200)
        return resp
    except Exception as e:
        resp = make_response({"Id" : 500,
                               "description": 
                               "Database was not loaded properly"},
                               500)
        return resp