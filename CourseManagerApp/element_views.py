from flask import Blueprint, flash, render_template, redirect, request, url_for, abort
from flask_login import login_required
from CourseManagerApp.dbmanager import get_db
from CourseManagerApp.element import Competency, Element, AddElementForm, EditElementForm

bp = Blueprint('elements', __name__, url_prefix='/elements/')

@bp.route('/', methods=['GET', 'POST'])
 
def get_elements():
    #form for creating an element
    form = AddElementForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            #get the competency with selected id
            try:
                competency = get_db().get_competency(form.competency_id.data)
            except Exception as e:
                print(e)
                flash('Something went wrong with the database. Competency was not selected.')
            
            element = Element(form.element_order.data,
                              form.element_name.data,
                              form.element_criteria.data,
                              competency)
            try:
                get_db().add_element(element)
                flash("Element was added successfully.")
                return redirect(url_for('elements.get_elements'))
            except Exception as e:
                print(e)
                flash('Something went wrong with the database. Element not added.')
        else:
            flash('Invalid input')
    #get elements matching search_word 
    search_word = None
    if request.args.get('search_word'):
        search_word = request.args.get('search_word')
        search_word = search_word.split('/')[0]
        try:
            elements, prev_page, next_page = get_db().get_elements(search_word=search_word)
            if len(elements) == 0:
                flash("The searched keyword was not found.")
        except Exception as e:
            flash('Database error. Could not find word.')

    #get all existing elements to display in a table if search_word is not provided
    else:
        try:
            elements, prev_page, next_page = get_db().get_elements()
        except Exception as e:
            print(e)
            flash('Could not load elements')
            abort(404)
        if not elements or len(elements)==0:
            abort(404)
    return render_template('elements.html', elements=elements, form=form)

    
@bp.route('/edit/<int:element_id>', methods=['GET', 'POST'])
@login_required
def edit_element(element_id):
    elem_to_edit = None
    try:
        elem_to_edit = get_db().get_element(element_id)
    except Exception as e:
            print(e)
            flash('Something went wrong with the database. Element could not be selected.')
    form = EditElementForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            #get the competency the user selected with its id from select field
            try:
                competency = get_db().get_competency(form.competency_id.data)
            except Exception as e:
                print(e)
                flash('Something went wrong with the database. Competency was not selected1.')
            
            element = Element(form.element_order.data,
                              form.element_name.data,
                              form.element_criteria.data,
                              competency)
            try:
                #edit the element
                get_db().edit_element(elem_to_edit, element)
                return redirect(url_for('elements.get_elements'))
            except Exception as e:
                print(e)
                flash('Something went wrong with the database. Element not edited.')
        else:
            flash('Invalid input')
    return render_template('edit_element.html', form=form, element_id=element_id, elem_to_edit=elem_to_edit)

@bp.route('/delete/<int:element_id>', methods=['GET', 'POST'])
@login_required
def delete_element(element_id):
    try:
        elem_to_delete = get_db().get_element(element_id)
        if elem_to_delete is None:
            flash("Element not found.")
        else:
            get_db().delete_element(elem_to_delete)
            flash("Element was successfully deleted.")
    except Exception as e:
        flash("Database Error. Element could not be deleted.")
    return redirect(url_for('elements.get_elements'))

@bp.route('/add/', methods=['GET', 'POST'])
def add_element():
    form = AddElementForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            #get the competency with selected id
            try:
                competency = get_db().get_competency(form.competency_id.data)
            except Exception as e:
                print(e)
                flash('Something went wrong with the database. Competency was not selected.')
            
            element = Element(form.element_order.data,
                              form.element_name.data,
                              form.element_criteria.data,
                              competency)
            try:
                get_db().add_element(element)
                return redirect(url_for('elements.get_elements'))
            except Exception as e:
                print(e)
                flash('Something went wrong with the database. Element not added.')
        else:
            flash('Invalid input')
    return render_template('create_element.html', form=form)


