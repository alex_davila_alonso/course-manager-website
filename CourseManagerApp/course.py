from .domain import Domain
from .term import Term
class Course:
    def from_json(course_dict):
        return Course(course_dict['course_id'],
            course_dict['course_title'],
            course_dict['theory_hours'],
            course_dict['lab_hours'],
            course_dict['work_hours'],
            course_dict['description'],
            course_dict['domain'],
            course_dict['term'],
            )
    def __init__(self, course_id, course_title, theory_hours, lab_hours, work_hours, description, domain, term):
        if (not isinstance(course_title, str) or course_title.isspace() or course_title == "" ):
            raise TypeError("Course title must be a non empty string")
        if not isinstance(theory_hours, int) or theory_hours < 0:
            raise TypeError("Theory hours must be an int and a positive number")
        if not isinstance(lab_hours, int) or lab_hours < 0:
            raise TypeError("Lab hours must be an int and a positive number")
        if not isinstance(work_hours, int) or work_hours < 0:
            raise TypeError("Work hours must be an int and a positive number")
        if (not isinstance(description, str) or description.isspace() or description == "" ):
            raise TypeError("Course description must be a non-empty string")
        if not isinstance(domain, Domain):
            raise TypeError("Domain input must be of type domain")
        if not isinstance(term, Term):
            raise TypeError("Term input must be of type term")
        self.id = course_id
        self.course_title = course_title
        self.theory_hours = theory_hours
        self.lab_hours = lab_hours
        self.work_hours = work_hours
        self.description = description
        self.domain = domain
        self.term = term
    
    def __repr__(self):
        return f'Course({self.id}, {self.course_title}, {self.theory_hours}, {self.lab_hours}, \
            {self.work_hours}, {self.description}, {self.domain}, {self.term})'
    
    def __str__(self):
        return f'<p> Course id: {self.id} </p> \
                <p> Course title: {self.course_title} </p>\
                <p> Theory hours: {self.theory_hours} </p> \
                <p> Lab hours: {self.lab_hours}</p> \
                <p> Work hours: {self.work_hours}</p> \
                <p> Description: {self.description}</p> \
                <p> Domain info: {self.domain}</p> \
                <p> Term info: {self.term}</p> '
                
    
    def to_json(self):
        return {
            "course_id": self.id,
            "course_title": self.course_title,
            "theory_hours": self.theory_hours,
            "lab_hours": self.lab_hours,
            "work_hours": self.work_hours,
            "description": self.description,
            "domain" : self.domain.to_json(),
            "term" : self.term.to_json()
        }
        
from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField,SelectField,Form
from wtforms.validators import DataRequired, ValidationError, Regexp,Length, NumberRange

def positive_hours(form, field):
    if field.data < 0:
        raise ValidationError('Hours must be positive or zero.')

   
class CourseForm(FlaskForm):
    id = StringField('Course ID', validators=[
        DataRequired(),
        Regexp(r'^\d{3}-\d{3}-[A-Z]{1,2}$', message="Invalid ID input")
    ])
    course_title = StringField('Course Title', validators=[DataRequired(), Length(max=50)])
    theory_hours = IntegerField('Theory Hours', validators=[DataRequired(), positive_hours])
    lab_hours = IntegerField('Lab Hours', validators=[DataRequired(), positive_hours])
    work_hours = IntegerField('Work Hours', validators=[DataRequired(), positive_hours])
    description = StringField('Description', validators=[DataRequired(), Length(max=1000)])
    domain = SelectField('Domain', validators=[DataRequired()], coerce=int)
    term = SelectField('Term', validators=[DataRequired()], coerce=int)

    def __init__(self, *args, **kwargs):
        super(CourseForm, self).__init__(*args, **kwargs)
        try:
            from .dbmanager import get_db
            self.domain.choices = [(d.domain_id,d.domain_name) for d in get_db().get_domains()]
            self.term.choices = sorted([(t.id,str(t.id)+" "+t.term_name) for t in get_db().get_terms()])
        except Exception:
            print('An error occured when loading the db')
            self.domain.choices = []
            self.term.choices = []