class Term:
    def __init__(self, term_name, term_id):
        if not isinstance(term_id, int) or (term_id < 0):
            raise TypeError("Term id must be of type int and be bigger than 0")
        if (not isinstance(term_name, str) or term_name.isspace() or term_name == "" ):
            raise TypeError("Term name must be a non-empty string")
        self.id = term_id
        self.term_name = term_name
        
    
    def __repr__(self):
        return f'Term({self.id}, {self.term_name})'
    
    def __str__(self):
        return f'<p> Term id: {self.id} </p>\
                <p> Term name: {self.term_name} </p> '
    
    def to_json(self):
        return{
            "term": self.id,
            "term_name":self.term_name
        }

from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField
from wtforms.validators import DataRequired
class CourseElementForm(FlaskForm):
    term_id = IntegerField('term_id', validators=[DataRequired()])
    term_name = StringField('term_name', validators=[DataRequired()])
