import re
from flask import Blueprint, abort, jsonify, request, flash, make_response, url_for
from .course import Course
from .element import Element
from .dbmanager import get_db
bp = Blueprint('courses_api', __name__, url_prefix='/api/courses')

@bp.route('', methods=['GET', 'POST'])
def courses_api():
    resp = None
    courses, prev_page, next_page = get_db().get_courses(page_num=1,page_size=10)
    # try:
    #     courses = get_db().get_courses()
    # except Exception:
    #     flash("An error occured while loading the database")
    if request.method == 'POST':
        result = request.json
        if result:
            course_id_pattern = r"^\d{3}-\d{3}-[A-Z]{1,2}$"  # Your regex pattern
            course_id = result.get('course_id', '')

            if not re.match(course_id_pattern, course_id):
                resp = make_response({"Id": 400, "description": "Invalid course ID"}, 400)
                return resp
            for course in courses:
                if result["course_title"] == course.course_title:
                    resp = make_response({"Id": 400, "description": "A course already exists with that name"}, 400)
                    return resp
            
            result['domain'] = get_db().get_domain(result['domain'])
            result['term'] = get_db().get_term(result['term'])
            course = Course.from_json(result)
            get_db().add_course(course)
        else:
            resp = make_response({"Id" : 400,
                              "description": 
                              "Error adding the course"},
                              400)
    else:
        page_num=1
        if request.args:
            page = request.args.get("page")
            if page:
                page_num = int(page)
        courses, prev_page, next_page = get_db().get_courses(page_num=page_num,page_size=10)
    next_page_url = None
    prev_page_url = None
    if prev_page:
        prev_page_url = url_for('courses_api.courses_api', page=prev_page)
    if next_page:
        next_page_url = url_for('courses_api.courses_api', page=next_page)

            # for course in courses:
            #     if course.course_title == title:
            #         return jsonify(course.__dict__)
    json_courses = {'next_page': next_page_url, 'prev_page': prev_page_url, 'results': [course.to_json() for course in courses]}
    if request.method == 'POST':
        resp = make_response(json_courses, 201)
    else:
        resp = make_response(json_courses, 200)
    return resp

@bp.route('/<string:course_id>')
def course_api(course_id):
    resp = None
    try:
        course = get_db().get_course(course_id)
        if not course:
            resp = make_response({"Id" : 404,
                              "description": 
                              "Course does not exist"},
                              404)
        else:
            json_course = course.to_json()
            resp = make_response(json_course, 200)
    except:
        resp = make_response({"Id" : 500,
                              "description": 
                              "Database was not loaded properly"},
                              500)
    return resp

@bp.route('/<string:course_id>', methods=['PUT'])
def update_api(course_id):
    resp = None
    try:
        see_if_exists = get_db().get_course(course_id);
        if not see_if_exists:
            resp = make_response({"Id" : 404,
                              "description": 
                              "Course does not exist"},
                              404)
        else:
            data = request.json
            data['domain'] = get_db().get_domain(data['domain'])
            data['term'] = get_db().get_term(data['term'])
            course = Course.from_json(data)
            get_db().update_course(course)
            json_course = course.to_json()
            resp = make_response(json_course, 200)
    except:
        resp = make_response({"Id" : 500,
                              "description": 
                              "Error loading the Database"},
                              500)
    return resp

@bp.route('/<string:course_id>',methods=['DELETE'])
def delete_api(course_id):
    see_if_exists = get_db().get_course(course_id);
    if not see_if_exists:
        resp = make_response({"Id" : 404,
                              "description": 
                              "Course does not exist"},
                              404)
    # try:
    #     courses = get_db().get_courses()
    # except Exception:
    #     flash("An error occured while loading the database")
    # json_courses = [course.to_json() for course in courses]
    else:
        get_db().delete_course(course_id)
        resp = make_response({"Id" : 204,
                            "description": 
                            f"Course {course_id} was successfully deleted"},
                              204)
    # resp = make_response(json_courses, 200)
    return resp

#Below will the the routes for the connection between courses and elements
@bp.route('/<string:course_id>/elements', methods=['GET'])
def course_elements_api(course_id):
    resp = None
    try:
        course_elements = get_db().get_course_elements(course_id)
        course = get_db().get_course(course_id)
        if not course_elements:
            json_course = course.to_json()
            resp = make_response(json_course,200)
        if not course:
            resp = make_response({"Id" : 404,
                              "description": 
                              "Course does not exist"},
                              404)
        else:
            json_elements = [element.to_json() for element in course_elements]
            json_course = course.to_json()
            json_course['element'] = json_elements
            resp = make_response({
                "course":json_course
            },200)
    except:
        resp = make_response({"Id" : 404,
                              "description": 
                              "Course does not exist"},
                              404)
    return resp


@bp.route('/<string:course_id>/elements', methods=['POST'])
def add_course_elements(course_id):
    result = request.json
    resp = None
    if result:
        try:
            course = get_db().get_course(course_id)
            if not course:
                resp = make_response({"Id" : 404,
                              "description": 
                              "Course does not exist"},
                              404)
            else:
                competency = get_db().get_competency(request.json["competency_id"])
                element = Element(request.json["element_order"],request.json["element_name"],request.json["element_criteria"],competency)
                #see if course already has the element
                course_elems = get_db().get_course_elements(course_id)
                already_exists = False
                #check if the element exists, so that the same element is not added twice for a course
                for elem in course_elems:
                    if (element.id == elem.id or element.element_name == elem.element_name or element.element_criteria == elem.element_criteria):
                        already_exists = True
                if not already_exists:
                    new_elem = get_db().add_element(element)
                    total_hours = (course.lab_hours + course.theory_hours)*15
                    get_db().add_course_element(new_elem.id,course_id,total_hours)

                    course_elements = get_db().get_course_elements(course_id)
                
                    json_elements = [element.to_json() for element in course_elements]
                    json_course = course.to_json()
                    json_course['element'] = json_elements
                    resp = make_response({'message': 'element successfully added'}, 201)
                    return resp

                else:
                    resp = make_response({"Id" : 409,
                    "description": 
                    "Course already has this element."},
                    409)
        except:
            resp = make_response({"Id" : 500,
                "description": 
                "Error loading the Database"},
                500)
    return resp


@bp.route('/<string:course_id>/elements/<string:competency_id>', methods=['GET'])
def course_element_competency_api(course_id, competency_id):
    try: 
        competency = get_db().get_competency(competency_id)
        json_competency = competency.to_json()
        resp = make_response(json_competency, 200)
        return resp
    except Exception as e:
        resp = make_response({"Id" : 500,
                              "description": 
                              "Error loading the Database"},
                              500)
    resp = make_response({"Id" : 500,
                              "description": 
                              f"{competency_id}"},
                              200)
    return resp

@bp.route('/<string:course_id>/elements/<string:element_id>', methods=['DELETE'])
def delete_course_elements(course_id, element_id):
    resp = None
    try:
        course = get_db().get_course(course_id)
        if not course:
            resp = make_response({"Id" : 404,
                            "description": 
                          "Course does not exist"},
                          404)
        else:
            element_id = int(element_id)
            element = get_db().get_element(element_id)
            #see if course already has the element
            course_elems = get_db().get_course_elements(course_id)
            exists = False
            #check if the element exists before deleting
            for elem in course_elems:
                if (element.id == elem.id or element.element_name == elem.element_name or element.element_criteria == elem.element_criteria):
                    exists = True
            #if it exists, it can be deleted
            if exists:
                    get_db().delete_course_element(element.id,course_id)
                    resp = make_response({'message': 'element successfully deleted'}, 204)
                    return resp
                #if it doesn't exists, error response is returned
            else:
                    resp = make_response({"Id" : 409,
                    "description": 
                    "The course does not have this element"},
                    409)
    except Exception as e:
            resp = make_response({"Id" : 500,
                "description": 
                "Error loading the Database"},
                500)
    return resp