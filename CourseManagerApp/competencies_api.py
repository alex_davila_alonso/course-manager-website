from flask import Blueprint, jsonify, request, url_for, make_response 
from CourseManagerApp.competency import Competency
from CourseManagerApp.dbmanager import get_db


bp = Blueprint('competencies_api', __name__, url_prefix='/api/competencies')
@bp.route('/', methods=['GET', 'POST'])
def competencies_api():
    competencies = None
    next_page_url = None
    prev_page_url = None
    
    if request.method == 'POST':
        result = request.json
        if result:
            competency = Competency.from_json(result)
            try:
                get_db().add_competency(competency)
                resp = make_response({'message': 'competency successfully added'}, 201)
                return resp
            except Exception as e:
                resp = make_response({'error': 'competency not added'}, 500)
                return resp
                
    else:
        page_num = 1
        if request.args:
            page = request.args.get("page")
            if page:
                page_num = int(page)
        try:
            competencies, prev_page, next_page = get_db().get_competencies(page_num=page_num, page_size=5)
        except Exception as e:
            resp = make_response({}, 500)
            return resp
        if prev_page:
            prev_page_url = url_for('competencies_api.competencies_api', page=prev_page)
        if next_page:
            next_page_url = url_for('competencies_api.competencies_api', page=next_page)
    
        json_competencies = {'next_page': next_page_url, 'prev_page':prev_page_url, 'results': [competency.__dict__ for competency in competencies]}
        return jsonify(json_competencies) 

@bp.route('/<string:competency_id>', methods=['GET', 'PUT', 'DELETE'])
def competency_api(competency_id):
    #check if competency_id exits in the db
    try:
        competency = get_db().get_competency(competency_id)
        if competency is None:
            resp = make_response({'error': 'competency not found'}, 404)
            return resp
    except Exception as e:
        print(e)
    if request.method == 'PUT':
        try:
            data = request.json
            competency = Competency.from_json(data)
            #get the old competency
            comp_old = get_db().get_competency(competency_id)
            #update it
            get_db().edit_competency(comp_old, competency)
            resp = make_response({'message': 'competency successfully updated'}, 201)
            return resp
        except Exception as e:
            print(e)
            resp = make_response({'error': 'competency not updated'}, 500)
            return resp
    if request.method == 'DELETE':
        try:
            get_db().delete_competency(competency)
            resp = make_response({'message': 'competency successfully deleted'}, 204)
            return resp
        except Exception as e:
            resp = make_response({'error': 'competency not deleted'}, 400)
            return resp
    try: 
        competency = get_db().get_competency(competency_id)
        #if competency is found, return it in json format
        if competency is not None:
            json = competency.__dict__
            return jsonify(json)
        #if competency is not found, return error json
        else:
            resp = make_response({'error': 'competency not found'}, 404)
            return resp
    except Exception as e:
        print(e)
        resp = make_response({'error': 'competency not found'}, 500)
        return resp
        
            
