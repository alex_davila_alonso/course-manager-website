from flask import Blueprint, jsonify, request, url_for, make_response
from CourseManagerApp.element import Element, Competency
from CourseManagerApp.dbmanager import get_db

bp = Blueprint('elements_api', __name__, url_prefix='/api/elements')

@bp.route('/', methods=['GET', 'POST'])
def elements_api():
    elements = None
    next_page_url = None
    prev_page_url = None

    if request.method == 'POST':
        result = request.json
        if result:
            try:
                #get the competency from db to add to element object
                competency = get_db().get_competency(result['competency_id'])
                element = Element(result['element_order'], result['element_name'], result['element_criteria'], competency)
                get_db().add_element(element)
                resp = make_response({'message': 'element successfully added'}, 201)
                return resp
            except Exception as e:
                resp = make_response({'error': 'element not added'}, 500)
                return resp
    else:
        page_num = 1
        if request.args:
            page = request.args.get("page")
            if page:
                page_num = int(page)
        try:
            elements, prev_page, next_page = get_db().get_elements(page_num=page_num, page_size=5)
        except Exception:
            resp = make_response({'error': 'Database encoutered an error, elements could not be loaded.'}, 500)
            return resp
        if prev_page:
            prev_page_url = url_for('elements_api.elements_api', page=prev_page)
        if next_page:
            next_page_url = url_for('elements_api.elements_api', page=next_page)

        json_elements = {'next_page': next_page_url, 'prev_page': prev_page_url, 'results': [element.to_json() for element in elements]}
        return jsonify(json_elements)

@bp.route('/<int:element_id>', methods=['GET', 'PUT', 'DELETE'])
def element_api(element_id):
    #check if element exits in the db
    try:
        element = get_db().get_element(element_id)
        if element is None:
            resp = make_response({'error': 'element not found'}, 404)
            return resp
    except Exception as e:
        resp = make_response({'error': 'Database encountered an error.'}, 404)
        return resp
    if request.method == 'PUT':
        try:
            data = request.json
            competency = get_db().get_competency(data['competency_id'])
            element = Element(data['element_order'], data['element_name'], data['element_criteria'], competency)
            #get the old element
            elem_old = get_db().get_element(element_id)
            #update it
            get_db().edit_element(elem_old, element)
            resp = make_response({'message': 'element successfully updated'}, 201)
            return resp
        except Exception as e:
            resp = make_response({'error': 'element not updated'}, 500)
            return resp
    if request.method == 'DELETE':
        try:
            get_db().delete_element(element)
            resp = make_response({'message': 'element successfully deleted'}, 204)
            return resp
        except Exception as e:
            resp = make_response({'error': 'element not deleted'}, 400)
            return resp
    try: 
        element = get_db().get_element(element_id)
        #if element is found, return it in json format
        if element is not None:
            json = element.to_json()
            return jsonify(json)
        #if element is not found, return error json
        else:
            resp = make_response({'error': 'element not found'}, 404)
            return resp
    except Exception as e:
        resp = make_response({'error': 'element not found'}, 500)
        return resp


