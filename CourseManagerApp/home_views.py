# home_views.py
from flask import (Blueprint, render_template)
from CourseManagerApp.auth_views import get_members
from CourseManagerApp.dbmanager import get_db
bp = Blueprint('home_view', __name__, url_prefix='/')

@bp.route("/")
def index():
  members = get_db().get_all_member_users()
  admin_users = get_db().get_all_user_admins()
  admins = get_db().get_all_admins()

  return render_template('index.html', members=members, admins=admins, admin_users=admin_users)