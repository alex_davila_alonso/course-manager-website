from flask import Blueprint, abort, flash, redirect, render_template, request, url_for
from .course import Course, CourseForm
from .dbmanager import get_db

bp = Blueprint('courses',__name__,url_prefix='/courses/')

@bp.route('/',methods=['GET','POST'])
def get_courses():
    page_num = 1
    form = CourseForm()
    try:
        courses, prev_page, next_page = get_db().get_courses(page_num=page_num,page_size=10)
    except Exception:
        flash('An error occured when loading the courses!')
        abort(404)
    if request.method == 'POST':
        if form.validate_on_submit():
            # Test id until we figure out how to do it
            domain = get_db().get_domain(form.domain.data)
            term = get_db().get_term(form.term.data)
            new_course = Course(form.id.data,form.course_title.data,form.theory_hours.data,form.lab_hours.data,form.work_hours.data,
            form.description.data, domain, term)
            for course in courses:
                if new_course.course_title == course.course_title or new_course.id == course.id:
                    flash('Course already exists!')
                    return redirect(url_for('courses.get_courses'))
            get_db().add_course(new_course)
            courses.append(new_course)
            flash('Course added successfully')
        elif request.form.get('delete_course'):
            course_id = request.form.get('delete_course')
            get_db().delete_course(course_id)
            flash('Course deleted successfully')
            return redirect(url_for('courses.get_courses'))

        else:
            flash('Invalid input')
    else:
        # page_num=1
        if request.args:
            page = request.args.get("page")
            if page:
                page_num = int(page)

        search_word = None
        if request.args.get('search_word'):
            search_word = request.args.get('search_word')
            courses, prev_page, next_page = get_db().get_courses(page_num=page_num, page_size=10, search_word=search_word)
        else:
            courses, prev_page, next_page = get_db().get_courses(page_num=page_num,page_size=10)

    next_page_url = None
    prev_page_url = None
    if prev_page:
        prev_page_url = url_for('courses.get_courses', page=prev_page)
    if next_page:
        next_page_url = url_for('courses.get_courses', page=next_page)

            # for course in courses:
            #     if course.course_title == title:
            #         return jsonify(course.__dict__)
    return render_template('courses.html',courses=courses, form=form, prev_page=prev_page, next_page=next_page,\
         page=page_num, prev_page_url=prev_page_url, next_page_url=next_page_url)

@bp.route('/<string:course_id>')
def get_course(course_id):
    try:
        course = get_db().get_course(course_id)
        if not course:
            abort(404)
    except:
        flash('Error while loading the course')
    return render_template('one_course.html',course=course)

@bp.route('/edit/<string:course_id>', methods=['GET','POST'])
def edit_course(course_id):
    form = CourseForm()
    try:
        course = get_db().get_course(course_id)
        example_course = get_db().get_course(course_id)
    except:
        flash('Error retrieving desired course')
        return redirect(url_for('courses.get_courses'))
    if request.method == 'POST':
        if form.validate_on_submit():
            course.id = form.id.data
            course.course_title = form.course_title.data
            course.theory_hours = form.theory_hours.data
            course.lab_hours = form.lab_hours.data
            course.work_hours = form.work_hours.data
            course.description = form.description.data
            course.domain = get_db().get_domain(form.domain.data)
            course.term = get_db().get_term(form.term.data)
            get_db().update_course(course)
            flash('Course updated successfully')
            return redirect(url_for('courses.get_courses'))
        else:
            flash('Invalid course input')
    return render_template('edit_course.html', form=form, course=course, example=example_course)