from flask import Blueprint, abort, flash, redirect, render_template, request, url_for
from flask_login import login_required
from .domain import Domain, DomainForm
from .dbmanager import get_db

bp = Blueprint('domains',__name__,url_prefix='/domains/')

@bp.route('/',methods=['GET','POST'])
def get_domains():
    form = DomainForm()
    try:
        domains = get_db().get_domains()
    except Exception:
        flash('An error occured when loading the domains!')
    if request.method == 'POST':
        if form.validate_on_submit():
            # Test id until we figure out how to do it
            new_domain = Domain(form.domain_name.data,form.domain_description.data)
            for domain in domains:
                if new_domain.domain_name == domain.domain_name:
                    flash('Domain already exists!')
                    return redirect(url_for('domains.get_domains'))
            dom = get_db().add_domain(new_domain)
            domains.append(dom)
            flash('Domain added successfully')
        elif request.form.get('delete_domain'):
            domain_id = request.form.get('delete_domain')
            get_db().delete_domain(domain_id)
            flash('Domain deleted successfully')
            return redirect(url_for('domains.get_domains'))
        else:
            flash('Invalid domain input')
    return render_template('domains.html',domains=domains, form=form)

@bp.route('/edit/<int:domain_id>', methods=['GET','POST'])
@login_required
def edit_domain(domain_id):
    form = DomainForm()
    try:
        domain = get_db().get_domain(domain_id)
        example_domain = get_db().get_domain(domain_id)
    except:
        flash('Error retrieving desired domain')
    if request.method == 'POST':
        if form.validate_on_submit():
            domain.domain_name = form.domain_name.data
            domain.domain_description = form.domain_description.data
            get_db().update_domain(domain)
            flash('Domain updated successfully')
            return redirect(url_for('domains.get_domains'))
        else:
            flash('Invalid domain input')
    return render_template('edit_domain.html', form=form, updated_domain=domain, example=example_domain)